package com.projeto.transaction.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.transaction.models.Transaction;
import com.projeto.transaction.service.TransactionService;

@RestController
@RequestMapping(value = "/transaction")
public class TransactionController {

	@Autowired
	public TransactionService transacationService;

	@GetMapping("/all/")
	public List<Transaction> findAll() {
		return  transacationService.findAll();
	}

	@PutMapping("/")
	public Transaction save(@RequestBody @Valid Transaction transaction) {
		return transacationService.save(transaction);
	}

	@PostMapping("/")
	public Transaction atualiza(@RequestBody Transaction transaction) {
		return transacationService.save(transaction);
	}
}

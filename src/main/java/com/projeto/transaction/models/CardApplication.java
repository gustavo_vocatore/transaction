package com.projeto.transaction.models;

public enum CardApplication {

	DEBITO("DEBITO"), CREDITO("CREDITO"), VOUCHER("VOUCHER");

	private String value;

	CardApplication(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
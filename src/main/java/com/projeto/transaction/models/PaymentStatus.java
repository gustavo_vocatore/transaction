package com.projeto.transaction.models;

public enum PaymentStatus {

	INICIADO("INICIADO"), SUCCESS("SUCCESS"), PENDING("PENDING"), CANCELED("CANCELED"), FAILED("FAILED");

	private String value;

	PaymentStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
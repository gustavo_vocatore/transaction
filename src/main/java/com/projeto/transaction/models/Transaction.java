package com.projeto.transaction.models;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ARM_TRANSACTION")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private LocalTime time;
	private BigDecimal value;
	private LocalDate date;

	private CardApplication CardApplication;
	private PaymentStatus paymentStatus;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", time=" + time + ", value=" + value + ", date=" + date + ", CardApplication="
				+ CardApplication + ", paymentStatus=" + paymentStatus + "]";
	}

}

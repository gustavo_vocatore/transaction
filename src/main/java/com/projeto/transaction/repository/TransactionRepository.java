package com.projeto.transaction.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.projeto.transaction.models.Transaction;

public interface TransactionRepository  extends CrudRepository<Transaction  ,Long>{
	
	List<Transaction> findAll();

}
package com.projeto.transaction.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.transaction.models.Transaction;
import com.projeto.transaction.repository.TransactionRepository;

@Service
public class TransactionService {
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	public List<Transaction> findAll() {
		return transactionRepository.findAll();
	}

	public Transaction save(Transaction transcation) {
		return transactionRepository.save(transcation);
	}
}
